package com.learn.shiro.entity;

import java.io.Serializable;

public class ShiroUser implements Serializable {
    private Integer id;
    private String username;

    public ShiroUser() {
    }

    public ShiroUser(Integer id, String username) {
        this.id = id;
        this.username = username;
    }

    public ShiroUser(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
