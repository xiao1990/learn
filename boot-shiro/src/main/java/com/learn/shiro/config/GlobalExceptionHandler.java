package com.learn.shiro.config;

import com.learn.shiro.util.R;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * https://blog.csdn.net/baidu_22254181/article/details/80789076
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    public R unauthorHandler(HttpServletRequest request, Exception exception) throws Exception {
        return R.error(1403,"暂无该操作权限");
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R exceptionHandler(HttpServletRequest request, Exception exception) throws Exception {
        logger.error("e:", exception);
        return R.error();
    }
}