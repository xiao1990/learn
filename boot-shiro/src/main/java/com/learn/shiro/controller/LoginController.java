package com.learn.shiro.controller;

import com.learn.shiro.util.R;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * https://github.com/wuyouzhuguli/FEBS-Shiro
 */
@RestController
public class LoginController {
    @PostMapping("/login")
    public R login(String username, String password) {
       /* if (!StringUtils.isNotBlank(code)) {
            return R.error("验证码不能为空！");
        }*/
//        rememberMe = false;
        // 密码 MD5 加密
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            Subject subject = SecurityUtils.getSubject();
//            if (subject != null) subject.logout();
            subject.login(token);
            return R.ok();
        } catch (UnknownAccountException | IncorrectCredentialsException | LockedAccountException e) {
            return R.error(e.getMessage());
        } catch (AuthenticationException e) {
            return R.error("认证失败！");
        }
    }
    @GetMapping("/logout")
    public R logout() {
        SecurityUtils.getSubject().logout();
        return R.ok();
    }
}
