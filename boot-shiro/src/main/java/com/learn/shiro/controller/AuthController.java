package com.learn.shiro.controller;

import com.learn.shiro.util.R;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {
    @RequiresPermissions("aa")
    @GetMapping("auth")
    public R auth(){
        return R.ok();
    }

    @GetMapping("auth2")
    @RequiresPermissions("cc")
    public R noauth(){
        return R.ok();
    }

    @GetMapping("user")
    @RequiresPermissions("cc")
    public R user(){
        System.out.println(SecurityUtils.getSubject().getPrincipals().asList().toString());
        System.out.println(SecurityUtils.getSubject().getPrincipal());
        System.out.println(SecurityUtils.getSubject().hasRole("aa"));
        System.out.println(SecurityUtils.getSubject().isPermitted("aa"));
        System.out.println(SecurityUtils.getSubject().isPermitted("cc"));
//        System.out.println(SecurityUtils.getSubject().getPrincipals().);
        return R.ok();
    }

    @GetMapping("/403")
    public R unauth(){
        return R.error(1403,"无该权限");
    }

    @GetMapping("/aaa")
    public R aaa(){
        return R.error(1403,"无该权限");
    }

}
