#### 开发计划

  整合模块  | 功能描述 | 期限	| 状态    
---|---|---|---    
Gateway+Jwt|整合鉴权中心|2020-1-08|2020-1-03
RocketMQ|消息队列|2020-1-14|2020-1-06
Nacos-Config|动态配置中心|2020-1-20|2020-1-07
JPA+Mybatis-plus|整合ORM底层.前端控制sort排序|2020-1-10|-
Spring Security+Oauth2.0|整合认证中心|2020-1-16|-
完善RocketMq|一对多消费者,发送邮件、服务号推送|2020-1-21

#### 其他要点
  整合模块  | 功能描述 | 备注	| 状态    
---|---|---|---    
docker/k8s|容器化部署|-|-
-|关系型数据库集群|主从备份、读写分离|-
Fescar/Seata|分布式事务解决方案|阿里|
-|日志系统|-|-
-|服务监控|-|-
nuxt.js|SEO优化检索|-|-

[nuxt.js简述](https://www.jianshu.com/p/b0626ba924c9)