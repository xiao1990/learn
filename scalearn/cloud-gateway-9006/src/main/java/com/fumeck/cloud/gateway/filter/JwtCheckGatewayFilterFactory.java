package com.fumeck.cloud.gateway.filter;

import com.alibaba.fastjson.JSON;
import com.fumeck.cloud.core.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

/**
 * @Classname JwtCheckGatewayFilterFactory
 * @Description TODO
 * @Date 2020/1/3 16:28
 * @Created by ZEWEN-PC
 */
@Slf4j
@Component
public class JwtCheckGatewayFilterFactory extends AbstractGatewayFilterFactory<Object> {
    @Override
    public GatewayFilter apply(Object config) {
        return (exchange, chain) -> {
            String token = exchange.getRequest().getHeaders().getFirst("Authentication");
            String path = exchange.getRequest().getURI().getPath();
            log.error("path------------{}", path);
            if (token != null) {
                return chain.filter(exchange);
            }
            ServerHttpResponse resp = exchange.getResponse();
            return authErro(resp, "请登陆1");
        };
    }

    /**
     * 认证错误输出
     *
     * @param resp 响应对象
     * @param mess 错误信息
     * @return
     */
    private Mono<Void> authErro(ServerHttpResponse resp, String mess) {
        resp.setStatusCode(HttpStatus.UNAUTHORIZED);
        resp.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        String returnStr = JSON.toJSONString(R.error(HttpStatus.UNAUTHORIZED.value(), mess));
        DataBuffer buffer = resp.bufferFactory().wrap(returnStr.getBytes(StandardCharsets.UTF_8));
        return resp.writeWith(Flux.just(buffer));
    }
}
