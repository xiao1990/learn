## Spring Cloud Alibaba
项目名称    | 编号 | 名称	| 说明    
---|---|---|---    
Nacos|-|服务注册与发现中心|阿里巴巴开源的动态服务发现
Nacos|-|配置管理|阿里巴巴开源的动态配置管理
Ribbon|-|负载均衡|Netflix成员
Feign|-|声明式服务调用|Netflix成员
Sentinel|-|熔断限流|Alibaba成员,取代Netflix的Hystrix
Gateway|-|网关|Spring Cloud自家的,取代Netflix的zuul

[新一代的微服务架构:Spring cloud Alibaba](https://www.cnblogs.com/lykbk/p/dfdfddf3434343434.html)    
程序员DD：[个人官网](http://blog.didispace.com)     
简书-[《Spring Cloud Alibaba基础教程》连载目录](https://www.jianshu.com/p/9c98eb1088a3)
### Nacos
一个更易于构建云原生应用的动态服务发现、配置管理和服务管理平台
- 动态配置服务
    -  以中心化、外部化和动态化的方式管理所有环境的配置
    - 消除变更时需要重新部署应用和服务的需要
    - 实现无状态服务更简单？？
    - 弹性扩展服务更容易
- 服务发现及管理
    - 支持DNS和RPC两种服务调用方式
    - 心跳检测(更容易为服务实现断路器)
- 动态DNS服务
    - 支持权重路由
    - 中间层负载均衡、更灵活的路由策略、流量控制
    - 消除耦合到厂商私有服务发现API上的风险
    
#### 开发
参考示例:  
[Spring Cloud Alibaba之服务发现组件 - Nacos实现服务注册与发现（二）](https://www.jianshu.com/p/34ed86b25049)  
[Spring Cloud Alibaba基础教程：支持的几种服务消费方式（RestTemplate、WebClient、Feign）](https://www.jianshu.com/p/447143d91837)  

安装Nacos并启动  
Nacos官网:[https://nacos.io/zh-cn/](https://nacos.io/zh-cn/)  
下载地址:[https://github.com/alibaba/nacos/releases](https://github.com/alibaba/nacos/releases)  

- 引入常规的spring-boot依赖包
- 引入阿里巴巴的spring-cloud-starter-alibaba-nacos-discovery
    - 启动类添加@EnableDiscoveryClient
    - 配置application.yml把服务注册到Nacos(默认8848端口)中
    ```
    server:
      port: 9001 #服务端口
    spring:
      application:
        # 服务名称尽量用 -，不用用 _，不用用特殊字符
        name: alibaba-nacos-discovery-server
      cloud:
        nacos:
          discovery:
            # nacos server 地址
            server-addr: localhost:8848
    ```
- 负载均衡使用LoadBalancerClient.
    - 亦可采用@LoadBalanced注解式RestTemplate(屏蔽获取服务的具体实例并采用uri拼接的方式访问)
    - 负载均衡的内部实现细节是由Ribbon实现(Netflix的东西)
    - 可以自定义拓展负载均衡Ribbon的具体实现
        - 算法：随机|hash|轮询
        - 权重：通过实现AbstractLoadBalancerRule整合Nacos的权重规则 

#### Spring Cloud Gateway + Nacos
[集成Nacos实现动态路由](https://www.cnblogs.com/zlt2000/p/11712943.html)    
动态路由配置时(JSON方式存储在NACOS中)遇到几个问题  
- 找不到Nacos服务:必须新建配置文件bootstrap.properties
- 路径规则问题: 
```
{
     "id": "fumeck",
     "predicates": [{
         "name": "Path",
         "args": {
             "pattern": "/fumeck/**"
         }
     }],
     "uri": "https://www.fumeck.com/",
     "filters": [{"name":"StripPrefix","args":{"_genkey_0":"1"}}]
 }
```
访问ip:9006/fumeck时,会访问到`www.fumeck.com/fumeck`(我希望去掉'/fumeck')     
通过以下链接找到答案："filters": [{"name":"StripPrefix","args":{"_genkey_0":"1"}}],这里的含义是默认去掉第一层路径就相当于去掉fumeck进行访问   
[网关服务自定义路由规则(在Nacos中配置json的方式)](https://www.cnblogs.com/ruanjianlaowang/p/11713655.html)

##### Gateway+JWT
[spring Cloud Gateway + JWT 实现统一的认证授权](https://blog.csdn.net/github_35976996/article/details/95170542)

#### cloud Alibaba集成RocketMq
[spring-cloud-alibaba集成rocketmq](https://segmentfault.com/a/1190000019035239)   
[Apache RocketMQ 消息队列部署与可视化界面安装](https://www.cnblogs.com/zlt2000/p/11531880.html)
