package com.fumeck.cloud.mq.service;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RocketmqProducer {
    @Autowired
    private DefaultMQProducer defaultProducer;

    public void send(String message) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        Message msg = new Message("test-topic", "test-tag", message.getBytes());
        defaultProducer.send(msg);
    }
}