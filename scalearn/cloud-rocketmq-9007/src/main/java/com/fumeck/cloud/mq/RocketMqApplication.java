package com.fumeck.cloud.mq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;

/**
 * @Classname RocketMqApplication
 * @Description TODO
 * @Date 2020/1/6 10:34
 * @Created by ZEWEN-PC
 */
@SpringBootApplication
@EnableBinding({ Source.class, Sink.class })
public class RocketMqApplication {
    public static void main(String[] args) {
        SpringApplication.run(RocketMqApplication.class, args);
    }
}
