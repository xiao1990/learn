package com.fumeck.nacos.client.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * @Classname NacosDynamicConfig
 * @Description TODO
 * @Date 2020/1/7 16:46
 * @Created by ZEWEN-PC
 */
@ConfigurationProperties(prefix = "fumeck.dynamic")
@Configuration
@Setter
@Getter
@RefreshScope
public class NacosDynamicConfig {
    private String name;
}
