package com.fumeck.nacos.client.controller;

import com.fumeck.nacos.client.config.NacosDynamicConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Slf4j
@RestController
@RefreshScope
public class TestController {

    @Autowired
    private LoadBalancerClient loadBalancerClient;
    @Autowired
    private RestTemplate restTemplate;
    @Resource
    private NacosDynamicConfig nacosDynamicConfig;
    @Value("${fumeck.dynamic.name:}")
    private String name;

    @GetMapping("/name")
    public String name() {
        System.out.println(name);
        return name;
    }

    @GetMapping("/testConfig")
    public String testConfig() {
        System.out.println(nacosDynamicConfig.getName());
        return nacosDynamicConfig.getName();
    }

    /**
     * 通过负载均衡客户端获取服务列表并返回其一
     * 通过uri的方式拼接请求url
     *
     * @return
     */
    @GetMapping("/test")
    public String test() {
        // 通过spring cloud common中的负载均衡接口选取服务提供节点实现接口调用
        ServiceInstance serviceInstance = loadBalancerClient.choose("alibaba-nacos-discovery-server");
        String url = serviceInstance.getUri() + "/hello?name=" + "wolf";
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class);
        return "Invoke : " + url + ", return : " + result;
    }

    /**
     * Spring Cloud引入Ribbon配合 restTemplate 实现客户端负载均衡
     * 对比test函数优势：屏蔽内部获取服务器细节，直接使用服务名进行请求
     * 说白了就是加多一层抽象
     *
     * @return
     */
    @GetMapping("/test1")
    public String test1() {
        String result = restTemplate.getForObject("http://alibaba-nacos-discovery-server/hello?name=wolf", String.class);
        return "Return : " + result;
    }
}