package com.fumeck.cloud.sys.service;

import com.fumeck.cloud.sys.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统_用户 服务类
 * </p>
 *
 * @author X-man
 * @since 2020-01-10
 */
public interface ISysUserService extends IService<SysUser> {

}
