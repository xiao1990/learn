package com.fumeck.cloud.sys.service.impl;

import com.fumeck.cloud.sys.entity.SysUser;
import com.fumeck.cloud.sys.mapper.SysUserMapper;
import com.fumeck.cloud.sys.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统_用户 服务实现类
 * </p>
 *
 * @author X-man
 * @since 2020-01-10
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

}
