package com.fumeck.cloud.sys.mapper;

import com.fumeck.cloud.sys.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统_用户 Mapper 接口
 * </p>
 *
 * @author X-man
 * @since 2020-01-10
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
