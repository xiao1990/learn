package com.fumeck.cloud.core;

import java.util.HashMap;
import java.util.Map;

/**
 * 公用常量
 *
 * @author yang
 */
public class Const {
    // 默认验证码参数名称
    public static final String DEFAULT_CAPTCHA_PARAM = "captcha";
    //日期格式化
    public static final String DATE_FORMAT_MONTH = "yyyy-MM";
    public static final String DATE_FORMAT_DAY = "yyyy-MM-dd";
    public static final String DATE_FORMAT_MDATETIME = "yyyy-MM-dd HH:mm:ss";

    //http状态码
    public static final Integer HTTP_NO_LOGIN= 1001;
    public static final Integer HTTP_PARAM_ERROR= 1002; //参数异常
    public static final Integer HTTP_ERROR= 1101; //服务异常
    public static final Integer AUTH_OPENID_UNACCESS = 1201;
    // 订单当前状态下不支持用户的操作，例如商品未发货状态用户执行确认收货是不可能的。
    public static final Integer ORDER_INVALID_OPERATION = 1301;
    public static final Integer ORDER_COMMENTED = 1302;
    public static final Integer ORDER_COMMENT_EXPIRED = 1303;
    public static final Integer ORDER_PAY_FAIL = 1304;

    public static final Integer ORDER_REFUND_FAILED = 1621;
//    public static final Integer HTTP_MINI_NO_LOGIN = 401; //微信未授权登录
//    public static final Integer HTTP_MINI_NO_LINK= 402; //微信未关联账号
//    public static final Integer HTTP_STATUS_ERROR= 407; //会员状态异常
//    public static final Integer HTTP_MEMBER_NON= 408; //会员不存在
//    public static final Integer HTTP_LOGIN_FAIL= 409; //登录错误
//    public static final Integer HTTP_PAY_FAIL= 410; //支付密码错误
//    public static final Integer HTTP_NO_AUTH = 507;

    public static final Map<String,String> HTTP_MSG_MAP= new HashMap(){{
        put(HTTP_NO_LOGIN,"请登录");
        put(HTTP_PARAM_ERROR,"参数不对");
        put(ORDER_REFUND_FAILED,"订单退款失败");
    }};

    public static String tradeNo() {
        return System.currentTimeMillis() + getFour();
    }

    public static String getFour() {
        return (int) ((Math.random() * 9 + 1) * 1000) + "";
    }
}
