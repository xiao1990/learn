package com.fumeck.cloud.core.config;

import com.alibaba.fastjson.JSON;
import com.fumeck.cloud.core.Const;
import com.fumeck.cloud.core.util.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
@Order(value = Ordered.LOWEST_PRECEDENCE)
public class GlobalExceptionHandler {
    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public Map<String, Object> serviceExceptionHandler(ServiceException e, HttpServletRequest request) {
        log(e, request);
        Map<String, Object> map = new HashMap<>(2);
        map.put("code", e.getCode());
        map.put("msg", e.getMessage());
        return map;
    }


    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Map<String, Object> exceptionHandler(Exception e, HttpServletRequest request) {
        log(e, request);
        Map<String, Object> map = new HashMap<>(2);
        map.put("code", Const.HTTP_ERROR);
        map.put("msg", "服务开小差了");
        return map;
    }

    /**
     * 记录日志
     *
     * @param e
     * @param request
     */
    private void log(Exception e, HttpServletRequest request) {
        String uri = request.getRequestURI();
        String method = request.getMethod();
        String paramJson;
        if (method.equals("POST")) {
            paramJson = getRequestPostStr(request);
        } else {
            Map map = request.getParameterMap();
            paramJson = JSON.toJSONString(map);
        }

        logger.error("请求{}:{}\n 参数:{}", method, uri, paramJson, e);
    }

    /**
     * 描述:获取 post 请求的 byte[] 数组
     * <pre>
     * 举例：
     * </pre>
     *
     * @param request
     * @return
     * @throws IOException
     */
    private byte[] getRequestPostBytes(HttpServletRequest request)
            throws IOException {
        int contentLength = request.getContentLength();
        if (contentLength < 0) {
            return null;
        }
        byte buffer[] = new byte[contentLength];
        for (int i = 0; i < contentLength; ) {
            int readlen = request.getInputStream().read(buffer, i,
                    contentLength - i);
            if (readlen == -1) {
                break;
            }
            i += readlen;
        }
        return buffer;
    }

    /**
     * 描述:获取 post 请求内容
     * <pre>
     * 举例：
     * </pre>
     *
     * @param request
     * @return
     * @throws IOException
     */
    private String getRequestPostStr(HttpServletRequest request) {
        try {
            byte buffer[];
            buffer = getRequestPostBytes(request);

            String charEncoding = request.getCharacterEncoding();
            if (charEncoding == null) {
                charEncoding = "UTF-8";
            }
            return new String(buffer, charEncoding);
        } catch (IOException e) {
            logger.error("IOException", e);
            return null;
        }
    }

}
