package com.fumeck.cloud.auth.web;


import com.fumeck.cloud.core.util.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 系统_用户 前端控制器
 * </p>
 *
 * @author X-man
 * @since 2020-01-10
 */
@Controller
@RequestMapping("/sys/sys-user")
public class SysUserController extends BaseController {

}
