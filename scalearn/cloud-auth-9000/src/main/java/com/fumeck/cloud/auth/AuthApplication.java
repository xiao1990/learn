package com.fumeck.cloud.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Classname AuthApplication
 * @Description TODO
 * @Date 2020/1/2 16:25
 * @Created by ZEWEN-PC
 */
@SpringBootApplication
public class AuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }
}
