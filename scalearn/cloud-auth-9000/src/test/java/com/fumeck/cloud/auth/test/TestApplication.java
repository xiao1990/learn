package com.fumeck.cloud.auth.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @Classname a
 * @Description TODO
 * @Date 2019/7/4 10:22
 * @Created by ZEWEN-PC
 */
@SpringBootApplication(scanBasePackages = {"com.fumeck.cloud.core","com.fumeck.cloud.*.service"})
@MapperScan("com.fumeck.cloud.*.mapper")
public class TestApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }
}
