package com.fumeck.cloud.auth.test;

import com.alibaba.fastjson.JSON;
import com.fumeck.cloud.sys.entity.SysUser;
import com.fumeck.cloud.sys.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @Classname UserTest
 * @Description TODO
 * @Date 2020/1/10 14:56
 * @Created by ZEWEN-PC
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@Slf4j
public class UserTest {
    @Autowired
    private ISysUserService sysUserService;

    @Test
    public void saveTest() {
        SysUser user = new SysUser();
        user.setMobile("17665263201");
        user.setUsername("nima");
        user.setPassword("aad");
        sysUserService.save(user);
    }

    @Test
    public void test() {
        List<SysUser> userList = sysUserService.list();
        log.info(JSON.toJSONString(userList));
    }

}
