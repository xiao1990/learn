package com.fumeck.house.sys.mapper;

import com.fumeck.house.sys.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
