package com.fumeck.house.sys.service.impl;

import com.fumeck.house.sys.entity.SysRole;
import com.fumeck.house.sys.mapper.SysRoleMapper;
import com.fumeck.house.sys.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}
