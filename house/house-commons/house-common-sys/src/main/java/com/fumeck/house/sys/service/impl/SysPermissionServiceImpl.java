package com.fumeck.house.sys.service.impl;

import com.fumeck.house.sys.entity.SysPermission;
import com.fumeck.house.sys.mapper.SysPermissionMapper;
import com.fumeck.house.sys.service.SysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements SysPermissionService {

}
