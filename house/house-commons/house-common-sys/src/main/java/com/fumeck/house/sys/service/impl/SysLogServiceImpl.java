package com.fumeck.house.sys.service.impl;

import com.fumeck.house.sys.entity.SysLog;
import com.fumeck.house.sys.mapper.SysLogMapper;
import com.fumeck.house.sys.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

}
