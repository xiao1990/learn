package com.fumeck.house.sys.service;

import com.fumeck.house.sys.entity.SysPermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
public interface SysPermissionService extends IService<SysPermission> {

}