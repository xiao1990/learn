package com.fumeck.house.sys.service.impl;

import com.fumeck.house.sys.entity.SysAdmin;
import com.fumeck.house.sys.mapper.SysAdminMapper;
import com.fumeck.house.sys.service.SysAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员表 服务实现类
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
@Service
public class SysAdminServiceImpl extends ServiceImpl<SysAdminMapper, SysAdmin> implements SysAdminService {

}
