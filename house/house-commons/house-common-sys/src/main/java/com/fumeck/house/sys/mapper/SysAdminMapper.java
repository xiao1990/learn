package com.fumeck.house.sys.mapper;

import com.fumeck.house.sys.entity.SysAdmin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员表 Mapper 接口
 * </p>
 *
 * @author X-man
 * @since 2020-02-02
 */
public interface SysAdminMapper extends BaseMapper<SysAdmin> {

}
