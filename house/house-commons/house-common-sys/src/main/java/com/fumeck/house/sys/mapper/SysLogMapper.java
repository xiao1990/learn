package com.fumeck.house.sys.mapper;

import com.fumeck.house.sys.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 操作日志表 Mapper 接口
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
