package com.fumeck.house.sys.service;

import com.fumeck.house.sys.entity.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
public interface SysLogService extends IService<SysLog> {

}