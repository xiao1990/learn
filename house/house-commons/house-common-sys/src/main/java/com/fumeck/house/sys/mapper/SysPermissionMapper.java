package com.fumeck.house.sys.mapper;

import com.fumeck.house.sys.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

}
