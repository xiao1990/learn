package com.fumeck.house.sys.service;

import com.fumeck.house.sys.entity.SysAdmin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员表 服务类
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
public interface SysAdminService extends IService<SysAdmin> {

}