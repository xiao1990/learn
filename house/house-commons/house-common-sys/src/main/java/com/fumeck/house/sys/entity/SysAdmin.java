package com.fumeck.house.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.fumeck.house.core.util.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 管理员表
 * </p>
 *
 * @author X-man
 * @since 2020-02-02
 */
@Data
@Accessors(chain = true)
@TableName(autoResultMap = true)
public class SysAdmin extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 管理员名称
     */
    private String username;

    /**
     * 管理员密码
     */
    private String password;

    /**
     * 最近一次登录IP地址
     */
    private String lastLoginIp;

    /**
     * 最近一次登录时间
     */
    private Date lastLoginTime;

    /**
     * 头像图片
     */
    private String avatar;

    /**
     * 逻辑删除
     */
    /**
     * 删除标志（0代表存在1代表删除）
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private Boolean deleted;

    /**
     * 角色列表
     */
    @TableField(typeHandler = FastjsonTypeHandler.class)
    private List<Integer> roleIds;


}
