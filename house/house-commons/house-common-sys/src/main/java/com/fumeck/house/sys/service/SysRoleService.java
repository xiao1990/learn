package com.fumeck.house.sys.service;

import com.fumeck.house.sys.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
public interface SysRoleService extends IService<SysRole> {

}