package com.fumeck.house.core.util;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 业务框架基础实体
 * </p>
 *
 * @author jobob
 * @since 2018-10-21
 */
@Data
@Accessors(chain = true)
public class BaseEntity {
    /**
     * 关于TableLogic
     * https://www.cnblogs.com/HQ0422/p/10921462.html
     */
  /*  @TableLogic
    private Boolean deleted;

    @TableField(fill = FieldFill.INSERT)
    private String operator;
*/
    @JSONField(format = "yyyy-MM-dd HH:mm")
    @TableField(fill = FieldFill.INSERT)
    private Date addTime;
}
