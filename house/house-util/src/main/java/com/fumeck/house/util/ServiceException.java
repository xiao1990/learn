package com.fumeck.house.util;


/**
 * Created by xzwiaoen on 2017/8/23.
 */
public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 5162710183389028792L;

    public ServiceException() {
        super("服务异常.请联系管理员");
    }
    private int code = 500;
    /**
     * @param s 具体错误信息
     */
    public ServiceException(String s) {
        super(s);
    }

    public ServiceException(int code) {
        super(Const.HTTP_MSG_MAP.get(code));
        this.code = code;
    }

    public ServiceException(String message, int code) {
        super(message);
        this.code = code;
    }

    public ServiceException(String s, Throwable e) {
        super(s, e);
    }

    public int getCode() {
        return code;
    }
}
