package com.fumeck.house.test;

import com.alibaba.fastjson.JSON;
import com.fumeck.house.sys.entity.SysAdmin;
import com.fumeck.house.sys.entity.SysPermission;
import com.fumeck.house.sys.mapper.SysAdminMapper;
import com.fumeck.house.sys.service.SysAdminService;
import com.fumeck.house.sys.service.SysPermissionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @Classname ServiceTest
 * @Description TODO
 * @Date 2019/9/16 14:14
 * @Created by ZEWEN-PC
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
public class ServiceTest {
    private static Logger logger = LoggerFactory.getLogger(ServiceTest.class);
    @Autowired
    private SysAdminService sysAdminService;
    @Autowired
    private SysPermissionService sysPermissionService;

    @Test
    public void test1() {
       boolean b = sysAdminService.removeById(1);
        logger.info("{}",b);
    }

    @Test
    public void test2() {
        List<SysAdmin> admins = sysAdminService.list();
        logger.info(JSON.toJSONString(admins));
    }

    @Test
    public void test3() {
        List<SysPermission> permissions =  sysPermissionService.list();
        logger.info(JSON.toJSONString(permissions));
    }

}
