package com.fumeck.house.test.general;

import cn.hutool.crypto.digest.BCrypt;
import cn.hutool.crypto.digest.DigestUtil;

public class BcryptTest {
    public static void main(String[] args) {
        String a = DigestUtil.bcrypt("admin123");
        System.out.println(a);
        System.out.println(DigestUtil.bcryptCheck("admin123","$2a$10$WpsjcnLFIC5WVqDeSTEioOpZjSeAVMQqXD699KM7M94bKJVI3.r1u"));
    }
}
