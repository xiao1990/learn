package com.fumeck.house.admin.shiro;

import com.alibaba.fastjson.JSONObject;
import com.fumeck.house.admin.util.R;
import com.fumeck.house.admin.util.UserConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.filter.authc.UserFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Restful方式登陆<br>
 * 在参数中或者header里加参数login-token作为登陆凭证<br>
 * 参数值是登陆成功后的返回值中获取
 *
 * @author 小威老师 xiaoweijiagou@163.com
 * <p>
 * 2017年8月3日
 */
public class RestfulFilter extends UserFilter {

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if (HttpMethod.OPTIONS.name().equalsIgnoreCase(WebUtils.toHttp(request).getMethod())) {
            return Boolean.TRUE;
        }

        Subject subject = getSubject(request, response);
        // 如果其已经登录，再此发送登录请求
        if (null != subject && subject.isAuthenticated()) {
            return true;
        }
        //  拒绝，统一交给 onAccessDenied 处理
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) {
        String info = JSONObject.toJSONString(R.error(HttpStatus.UNAUTHORIZED.value(), "token不存在或者过期"));
        writeResponse(WebUtils.toHttp(response), HttpStatus.UNAUTHORIZED.value(), info);
        return false;
    }

    public static void writeResponse(HttpServletResponse response, int status, String json) {
        try {
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Methods", "*");
            response.setContentType("application/json;charset=UTF-8");
            response.setStatus(status);
            response.getWriter().write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
