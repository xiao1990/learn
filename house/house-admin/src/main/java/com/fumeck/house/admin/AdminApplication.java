package com.fumeck.house.admin;

import com.fumeck.house.admin.shiro.ShiroProperties;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * https://github.com/wuyouzhuguli/FEBS-Shiro
 */
@Slf4j
@SpringBootApplication(scanBasePackages = {"com.fumeck.house.admin","com.fumeck.house.core","com.fumeck.house.*.service"})
@EnableConfigurationProperties({ShiroProperties.class})
@MapperScan("com.fumeck.house.*.mapper")
public class AdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
        log.info("《《《《《《 FEBS started up successfully at {} {} 》》》》》》", LocalDate.now(), LocalTime.now());
    }
}
