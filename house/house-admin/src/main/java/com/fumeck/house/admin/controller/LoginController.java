package com.fumeck.house.admin.controller;

import com.alibaba.fastjson.JSON;
import com.fumeck.house.admin.util.R;
import com.fumeck.house.sys.service.SysAdminService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * https://github.com/wuyouzhuguli/FEBS-Shiro
 */
@Slf4j
@RestController
public class LoginController {
    @Autowired
    private SysAdminService sysAdminService;

    @PostMapping("/login")
    public R login(String username, String password) {
       /* if (!StringUtils.isNotBlank(code)) {
            return R.error("验证码不能为空！");
        }*/
        // 密码 MD5 加密\
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            Subject subject = SecurityUtils.getSubject();
//            if (subject != null) subject.logout();
            subject.login(token);
            return R.ok();
        } catch (UnknownAccountException | IncorrectCredentialsException | LockedAccountException e) {
            return R.error(e.getMessage());
        } catch (AuthenticationException e) {
            return R.error("认证失败！");
        }
    }
    @GetMapping("/logout")
    public R logout() {
        SecurityUtils.getSubject().logout();
        return R.ok();
    }
}
