package com.fumeck.house.admin.shiro;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "fumeck.shiro")
public class ShiroProperties {

    // shiro redis缓存时长，默认值 1800 秒
    private int expireIn = 1800;

    private String[] anonUrls;

    public int getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(int expireIn) {
        this.expireIn = expireIn;
    }

    public String[] getAnonUrls() {
        return anonUrls;
    }

    public void setAnonUrls(String[] anonUrls) {
        this.anonUrls = anonUrls;
    }
}
