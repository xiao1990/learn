package com.fumeck.house.admin.config;

import com.fumeck.house.admin.util.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * https://blog.csdn.net/baidu_22254181/article/details/80789076
 */
@Slf4j
@ControllerAdvice
public class AdminExceptionHandler {
    @ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    public R unauthorHandler(HttpServletRequest request, Exception exception) throws Exception {
        return R.error(1403,"暂无该操作权限");
    }

}