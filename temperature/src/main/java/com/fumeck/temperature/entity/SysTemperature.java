package com.fumeck.temperature.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fumeck.temperature.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 体温表
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class SysTemperature extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 姓名
     */
    private String realname;

    /**
     * 日期
     */
    @JSONField(format = "yyyy-MM-dd")
    private Date day;

    /**
     * 早上体温
     */
    private String morning;

    /**
     * 下午体温
     */
    private String night;

}
