package com.fumeck.temperature.service;

import com.fumeck.temperature.entity.SysTemperature;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 体温表 服务类
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
public interface SysTemperatureService extends IService<SysTemperature> {

    void saveTemperature(SysTemperature sysTemperature);
}
