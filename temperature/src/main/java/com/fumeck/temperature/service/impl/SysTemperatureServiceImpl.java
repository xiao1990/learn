package com.fumeck.temperature.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fumeck.temperature.entity.SysTemperature;
import com.fumeck.temperature.mapper.SysTemperatureMapper;
import com.fumeck.temperature.service.SysTemperatureService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 体温表 服务实现类
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
@Service
public class SysTemperatureServiceImpl extends ServiceImpl<SysTemperatureMapper, SysTemperature> implements SysTemperatureService {
    @Override
    public void saveTemperature(SysTemperature sysTemperature) {
        SysTemperature exist = baseMapper.selectOne(new QueryWrapper<SysTemperature>().lambda().eq(SysTemperature::getRealname,sysTemperature.getRealname()).eq(SysTemperature::getDay, DateUtil.formatDate(sysTemperature.getDay())));
        if(exist!=null){
            sysTemperature.setId(exist.getId());
            baseMapper.updateById(sysTemperature);
        }else{
            baseMapper.insert(sysTemperature);
        }
    }
}
