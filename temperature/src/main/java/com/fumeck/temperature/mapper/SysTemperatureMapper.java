package com.fumeck.temperature.mapper;

import com.fumeck.temperature.entity.SysTemperature;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 体温表 Mapper 接口
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
public interface SysTemperatureMapper extends BaseMapper<SysTemperature> {

}
