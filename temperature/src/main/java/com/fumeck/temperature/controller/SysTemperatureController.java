package com.fumeck.temperature.controller;


import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fumeck.temperature.core.R;
import com.fumeck.temperature.entity.SysTemperature;
import com.fumeck.temperature.service.SysTemperatureService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


/**
 * <p>
 * 体温表 前端控制器
 * </p>
 *
 * @author X-man
 * @since 2020-02-03
 */
@RestController
@RequestMapping("/sys/temperature/")
@Slf4j
public class SysTemperatureController{
    @Autowired
    private SysTemperatureService sysTemperatureService;

    @PostMapping("save")
    public R save(@RequestBody SysTemperature sysTemperature){
        sysTemperatureService.saveTemperature(sysTemperature);
        return R.ok();
    }

    @GetMapping("today")
    public R today(String realname, String day){
       SysTemperature sysTemperature = sysTemperatureService.getOne(new QueryWrapper<SysTemperature>().lambda().eq(SysTemperature::getRealname,realname).eq(SysTemperature::getDay, day));
        return R.putData(sysTemperature);
    }

    @GetMapping("list")
    public R list(String realname,String day){
        List<SysTemperature> sysTemperatures = sysTemperatureService.list(Wrappers.<SysTemperature>lambdaQuery().orderByDesc(SysTemperature::getId)
                .eq(StringUtils.isNotBlank(realname),SysTemperature::getRealname, realname)
                .eq(StringUtils.isNotBlank(day),SysTemperature::getDay, day));
        return R.putData(sysTemperatures);
    }

}
