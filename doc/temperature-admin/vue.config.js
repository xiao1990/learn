/**
 * vue-cli 3.x 配置文件 vue.config.js
 * https://www.jianshu.com/p/479c0978c048
 */
module.exports = {
  publicPath: './',
  outputDir: '/home/jxt-admin',
  assetsDir: 'static',
  lintOnSave: false,//取消ESlint验证
  devServer: {
    port: 8087,
    proxy: {
      // 配置跨域
      '/sys': {
        target: 'http://localhost:8088',
        ws: true, // 是否跨域
        changeOrigin: true
      }
    }
  }
};
