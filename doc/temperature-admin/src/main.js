import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import '@/styles/index.scss' // global css
import {
  Button,
  Input,
  Table,
  TableColumn,
  Loading,
  DatePicker
} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false;

Vue.use(Button);
Vue.use(Input);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Loading);
Vue.use(DatePicker);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");