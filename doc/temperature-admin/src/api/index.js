import request from '@/utils/request'

export function list(query) {
  return request({
    url: '/sys/temperature/list',
    method: 'get',
    params: query
  })
}