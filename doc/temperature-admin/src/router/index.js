import Vue from "vue";
import VueRouter from "vue-router";
import DataList from "../views/DataList.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "dataList",
    component: DataList
  }
];

const router = new VueRouter({
  // mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
