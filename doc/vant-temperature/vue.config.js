const path = require('path');

function resolve(dir = '') {
  return path.join(__dirname, './src', dir);
}
/**
 * vue-cli 3.x 配置文件 vue.config.js
 * https://www.jianshu.com/p/479c0978c048
 */
module.exports = {
  publicPath: './',
  outputDir: '/home/vant-temperature',
  assetsDir: 'static',
  productionSourceMap: false,
  lintOnSave: false,//取消ESlint验证
  devServer: {
    port: 8089,
    proxy: {
      // 配置跨域
      '/sys': {
        target: 'http://localhost:8088',
        ws: true, // 是否跨域
        changeOrigin: true
      }
    }
  },
  configureWebpack: {
    resolve: {
      alias: {
        core: resolve('core')
      }
    },
    optimization: {
      runtimeChunk: {
        name: entrypoint => `runtime~${entrypoint.name}`
      },
      splitChunks: {
        minChunks: 2,
        minSize: 20000,
        maxAsyncRequests: 20,
        maxInitialRequests: 30,
        name: false
      }
    }
  }
};
