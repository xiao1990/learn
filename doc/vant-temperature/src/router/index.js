import Vue from "vue";
import VueRouter from "vue-router";
import Tempurature from "../views/Temperature.vue";
import Record from "../views/Record.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "temperature",
    component: Tempurature
  }, {
    path: "/record",
    name: "record",
    component: Record
  }
];

const router = new VueRouter({
  // mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
