import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import '@/assets/scss/global.scss';

Vue.config.productionTip = false;

import {
  Icon,
  Cell,
  Button,
  Toast,
  NavBar,
  CellGroup,
  Field,
  Calendar,
  List,
  Panel
} from 'vant';
Vue.use(Icon);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(Button);
Vue.use(Toast);
Vue.use(Field);
Vue.use(NavBar);
Vue.use(Calendar);
Vue.use(List);
Vue.use(Panel);

/* 添加表单验证 */
/* import { ValidationProvider, extend } from 'vee-validate';
import { required } from 'vee-validate/dist/rules';
extend('required', {
  ...required,
  message: 'This field is required'
});
 */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");