import request from '@/utils/request'
const Save = '/sys/temperature/save'; //提交申请
export function save(data) {
  return request({
    url: Save,
    method: 'post',
    data
  })
}

const Today = '/sys/temperature/today'; //今天的数据
export function today(query) {
  return request({
    url: Today,
    method: 'get',
    params: query
  })
}

const List = '/sys/temperature/list'; //历史记录
export function list(query) {
  return request({
    url: List,
    method: 'get',
    params: query
  })
}


