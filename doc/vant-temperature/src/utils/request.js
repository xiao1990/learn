import axios from 'axios'
import { Dialog, Toast } from 'vant';

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // api 的 base_url
  timeout: 5000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    return config;
  },
  err => Promise.reject(err)
)

// response interceptor
service.interceptors.response.use(
  response => {
    const res = response.data
      return res
  }, error => {
    console.log('err' + error)// for debug
    Dialog.alert({
      title: '警告',
      message: '连接超时'
    });
    return Promise.reject(error)
  })

export default service
