package com.fumeck.core.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CoreController {
    @GetMapping("/test")
    public Object hello(){
        return "ok";
    }
}
