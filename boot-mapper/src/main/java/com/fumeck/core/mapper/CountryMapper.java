package com.fumeck.core.mapper;

import com.fumeck.core.model.Country;
import com.fumeck.core.util.MyMapper;


public interface CountryMapper extends MyMapper<Country> {
}