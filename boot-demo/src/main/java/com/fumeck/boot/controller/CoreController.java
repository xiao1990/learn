package com.fumeck.boot.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.fumeck.boot.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CoreController {
    @Autowired
    private ClientService clientService;

    @GetMapping("/")
    public Object hello(){
        return clientService.selectPage(new Page<>());
    }
}
