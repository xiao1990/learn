package com.fumeck.boot.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fumeck.boot.entity.Client;
import com.fumeck.boot.mapper.ClientMapper;
import com.fumeck.boot.service.ClientService;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl extends ServiceImpl<ClientMapper,Client> implements ClientService {

}
