package com.fumeck.boot.service;

import com.baomidou.mybatisplus.service.IService;
import com.fumeck.boot.entity.Client;

public interface ClientService extends IService<Client>{
}
