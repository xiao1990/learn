package com.fumeck.boot.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fumeck.boot.entity.Client;

public interface ClientMapper extends BaseMapper<Client> {

}