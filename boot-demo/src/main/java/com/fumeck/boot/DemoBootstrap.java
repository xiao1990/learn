package com.fumeck.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@MapperScan("com.fumeck.boot.mapper")
public class DemoBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(DemoBootstrap.class, args);
    }
}
