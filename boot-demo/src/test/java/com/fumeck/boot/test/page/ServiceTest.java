package com.fumeck.boot.test.page;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.plugins.Page;
import com.fumeck.boot.entity.Client;
import com.fumeck.boot.service.ClientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@MapperScan("com.fumeck.boot.mapper")
public class ServiceTest {
    private static Logger logger = LoggerFactory.getLogger(ServiceTest.class);
    @Autowired
    private ClientService clientService;
    @Test
    public void test(){
        List<Client> clients= clientService.selectList(null);
        logger.debug("**"+JSON.toJSONString(clients));
        logger.info("**"+JSON.toJSONString(clients));
    }

    @Test
    public void testPage(){
        Page<Client> clients= clientService.selectPage(new Page<>(1,2));
        logger.info("**"+JSON.toJSONString(clients));
    }
}
