package aa;

/**
 * https://blog.csdn.net/shavvn/article/details/80776462
 * <p>
 * Lucene Spatial构建地理空间索引：https://www.cnblogs.com/hanfight/p/5776769.html
 */
public class A {
    public static void main(String[] args) {
        aa("33", "44");
    }

    public static void aa(String longitude1, String latitude1) {
        double minlat = 0;//定义经纬度四个极限值。
        double maxlat = 0;
        double minlng = 0;
        double maxlng = 0;
//我数据保存的经纬度是string，所以要转一下类型方便计算距离。  shopParam是传参。
        double longitude = Double.parseDouble(longitude1);
        double latitude = Double.parseDouble(latitude1);

// 先计算查询点的经纬度范围
        double r = 6371;// 地球半径千米
        double dis = 20;// 距离(单位:千米)，查询范围 20km内的所有商铺
        double dlng = 2 * Math.asin(Math.sin(dis / (2 * r))
                / Math.cos(longitude * Math.PI / 180));
        dlng = dlng * 180 / Math.PI;// 角度转为弧度
        double dlat = dis / r;
        dlat = dlat * 180 / Math.PI;
        if (dlng < 0) {
            minlng = longitude + dlng;//拿到最大经度和最小经度
            maxlng = longitude - dlng;
        } else {
            minlng = longitude - dlng;
            maxlng = longitude + dlng;
        }
        if (dlat < 0) {
            minlat = latitude + dlat;//拿到最大纬度和最小纬度
            maxlat = latitude - dlat;
        } else {
            minlat = latitude - dlat;
            maxlat = latitude + dlat;
        }


        System.out.println("最大经度：" + maxlng + ",最小经度：" + minlng);
        System.out.println("最大纬度：" + maxlat + ",最小纬度：" + minlat);
       /* final List<Shop> shops = Lists.newArrayList();//定义一个空 list保存范围内的店铺
        final List<Shop> shopList = shopMapper.selectNearShopList();//查询所有商铺
        for (Shop shop:shopList) {
            double _long = Double.parseDouble(shop.getLongitude());//拿到店铺的坐标，判断是否在这个范围内
            double _lat = Double.parseDouble(shop.getLatitude());
            if (_long >= minlng && _long <= maxlng && _lat >= minlat && _lat <= maxlat){
                shops.add(shop);//将在这个范围内的店铺添加到一个空list中。
            }
        }
        Map<Shop,Double> map = new HashMap<Shop, Double>();//定义一个空的map存储店铺
//计算范围内每个店铺距离给定坐标的距离
        for (Shop _shop:shops) {
            Double distance = LocationUtils.getDistance(Double.parseDouble(_shop.getLatitude()),Double.parseDouble(_shop.getLongitude()),latitude,longitude);
            System.out.println(("店铺名称：{},经度：{},纬度：{},距离：{}",_shop.getShopName(),_shop.getLongitude(),_shop.getLatitude(),distance);
            map.put(_shop,distance);//将计算出来的距离作为value，该店铺作为key
        }
        final List<Shop> list = Lists.newArrayList();//存储排序之后的店铺
//这里使用Java8的map根据value排序，距离最近排序，按value排序，升序
        Map<Shop,Double> doubleMap = map
                .entrySet()
                .stream()
                .sorted(comparingByValue())
                .collect(
                        toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                                LinkedHashMap::new));
        for (Map.Entry<Shop, Double> entry:doubleMap.entrySet()) {
            list.add(entry.getKey());//循环map拿到key并保存到list中。
        }*/

    }
}
