//根据给定经纬度(lat,lng)求出其左上角(left_top),右上角(right_top),左下角(left_bottom)，右下角(right_bottom)的四个位置。所有在这个区域的范围都在该点附近。

public class Location {
    private double latitude;  
    private double longitude;  

    public Location(double latitude, double longitude) {
        this.latitude = latitude;  
        this.longitude = longitude;  
    }  

    public double getLatitude() {  
        return latitude;  
    }  

    public void setLatitude(double latitude) {  
        this.latitude = latitude;  
    }  

    public double getLongitude() {  
        return longitude;  
    }  

    public void setLongitude(double longitude) {  
        this.longitude = longitude;  
    }  

}