/**
 * 时间复杂度为n2的排序算法
 */
public class N2 {

    //冒泡排序
    public static void bubbleSort(int[] datas) {
        int length = datas.length;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (datas[i] > datas[j]) {
                    int temp = datas[i];
                    datas[i] = datas[j];
                    datas[j] = temp;
                }
            }
        }
    }

    //插入排序
    public static void inserSort(int[] datas) {
        int length = datas.length;
        for (int i = 1; i < length; i++) {
            int j = i - 1;
            int value = datas[i];
            for (; j >= 0; --j) {
                if (value > datas[j]) {
                    break;
                } else {
                    datas[j + 1] = datas[j];
                }
            }
            datas[j + 1] = value;
        }
    }

    //选择排序
    public static void selectSort(int[] datas) {
        int length = datas.length;
        for (int i = 0; i < length; i++) {
            int index = i;
            for (int j = i + 1; j < length; j++) {
                if (datas[j] < datas[index]) {
                    index = j;
                }
            }
            int value = datas[i];
            datas[i] = datas[index];
            datas[index] = value;
        }
    }

    public static void main(String[] args) {
        int[] datas = new int[]{5, 8, 3, 7, 13, 86, 12, 8};
        bubbleSort(datas);
        for (int i = 0; i < datas.length; i++) {
            System.out.println(datas[i]);
        }

        int[] b = new int[]{5, 8, 3, 7, 13, 86, 12, 8};
        inserSort(b);
        for (int i = 0; i < b.length; i++) {
            System.out.println(b[i]);
        }

        int[] c = new int[]{5, 8, 3, 7, 13, 86, 12, 8};
        selectSort(c);
        for (int i = 0; i < c.length; i++) {
            System.out.println(c[i]);
        }
    }
}
