/**
 * 循环队列
 */
public class LoopQueue {
    private int head = 0;//队头
    private int tail = 0;//队尾
    private int size;//数组大小
    private int[] queues;

    public LoopQueue(int size) {
        this.size = size;
        queues = new int[size];
    }

    //入队
    Boolean enqueue(int data) {
        if ((tail + 1) % size == head) {
            return false;
        }
        queues[tail] = data;
        tail = ++tail % size;
        System.out.println("tail:" + tail + "head:" + head+"-data:"+data);
        return true;
    }

    //出队
    Integer dequeue() {
        if (head == tail) {
            return null;
        }
        int data = queues[head];
        head = ++head % size;
        return data;
    }

    Boolean canEn() {
        return !((tail + 1) % size == head);
    }

    Boolean canDe() {
        return !(tail  == head);
    }
    public static void main(String[] args) {
        LoopQueue l = new LoopQueue(5);

        Thread t = new Thread(() -> {
             int i = 0;
            while (true) {
                if(l.canEn()) {
                    l.enqueue(++i);
                }else{
                   try {
                        Thread.sleep(5000);
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();

        Thread thread = new Thread(() -> {
            Integer data;
            while (true) {
                try {
                    Thread.sleep(1000);
                    if (l.canDe()) {
                        data = l.dequeue();
                        System.out.println(data + "-------------");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}