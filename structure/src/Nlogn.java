import java.util.Arrays;

/**
 *  时间复杂度为nlogn的排序算法
 */
public class Nlogn {
    //归并排序
    public static int[] mergeSort(int[] datas) {
        int length = datas.length;
        if (datas.length == 1) {
            return datas;
        }
        int p = length / 2;

        int[] left = mergeSort(Arrays.copyOfRange(datas, 0, p));
        int[] right = mergeSort(Arrays.copyOfRange(datas, p, length));
        return merge(left, right);
    }

    private static int[] merge(int[] left, int[] right) {
        int[] datas = new int[left.length + right.length];
        int i = 0;
        int lindex = 0;
        int rindex = 0;
        while (true) {
            if (left[lindex] > right[rindex]) {
                datas[i++] = right[rindex++];
                if (rindex == right.length) {
                    for (; lindex < left.length; lindex++) {
                        datas[i++] = left[lindex];
                    }
                }
            } else {
                datas[i++] = left[lindex++];
                if (lindex == left.length) {
                    for (; rindex < right.length; rindex++) {
                        datas[i++] = right[rindex];
                    }
                }
            }
            if (i == left.length + right.length) {
                break;
            }
        }
        return datas;
    }

    //快速排序
    public static void quickSort(int[] datas) {
        quickSort_1(datas, 0, datas.length - 1);
    }

    public static void quickSort_1(int[] datas, int start, int end) {
        if (start < end) {
            int point = datas[end];
            int i = start;
            for (int k = i; k < end; k++) {
                if (point >= datas[k]) {
                    int temp = datas[i];
                    datas[i] = datas[k];
                    datas[k] = temp;
                    i++;
                }
            }
            datas[end] = datas[i];
            datas[i] = point;
            quickSort_1(datas, start, i - 1);
            quickSort_1(datas, i + 1, end);
        }
    }

    public static void main(String[] args) {
        int[] datas = new int[]{5, 8, 3, 7, 13, 86, 12, 8};
        datas = mergeSort(datas);
        for (int i = 0; i < datas.length; i++) {
            System.out.println(datas[i]);
        }

        int[] a = new int[]{5, 8, 3, 7, 13, 86, 12, 8};
        quickSort(a);
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
}
