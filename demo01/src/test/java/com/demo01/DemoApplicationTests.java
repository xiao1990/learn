package com.demo01;

import com.demo01.service.TbUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class DemoApplicationTests {
    @Autowired
    private TbUserService tbUserService;

    @Test
    public void test() {
        System.out.println(tbUserService.getById(4));
    }
}
