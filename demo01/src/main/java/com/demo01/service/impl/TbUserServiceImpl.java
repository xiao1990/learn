package com.demo01.service.impl;

import com.demo01.dao.TbUserMapper;
import com.demo01.model.TbUser;
import com.demo01.service.TbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TbUserServiceImpl implements TbUserService{
    @Autowired
    private TbUserMapper tbUserMapper;

    @Override
    public TbUser getById(Integer id) {
        return tbUserMapper.selectByPrimaryKey(id);
    }
}
