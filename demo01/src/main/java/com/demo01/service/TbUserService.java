package com.demo01.service;

import com.demo01.model.TbUser;

import java.util.List;

public interface TbUserService {
    TbUser getById(Integer id);
}
