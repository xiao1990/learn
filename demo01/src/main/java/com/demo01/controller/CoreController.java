package com.demo01.controller;

import com.demo01.service.TbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RestController;

@RestController
public class CoreController {
    @Autowired
    private TbUserService tbUserService;

    @RequestMapping("/hello")
    public String hello() {
        return "-hello world-";
    }

    @GetMapping("/getById")
    public Object getById(Integer id){
        return  tbUserService.getById(id);
    }
}
