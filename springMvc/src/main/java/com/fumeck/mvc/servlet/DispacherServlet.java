package com.fumeck.mvc.servlet;

import com.fumeck.mvc.annotation.MyController;
import com.fumeck.mvc.annotation.MyRequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DispacherServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(DispacherServlet.class);
    private static final String packageName = "com.fumeck.mvc.controller";
    private List<String> classNames = new ArrayList<>();
    private List<String> mappings = new ArrayList<>();
    private Map<String, Method> methodMap = new HashMap<>();
    private Map<String, Object> iocMap = new HashMap<>();


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doReq(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doReq(req, resp);
    }

    private void doReq(HttpServletRequest req, HttpServletResponse resp) {
        String uri = req.getRequestURI();
        String url = req.getRequestURL().toString();
        System.out.println("url:" + url + "------uri:" + uri);
        System.out.println(uri.replace(url, ""));
        Method method = methodMap.get(uri);
        if (method != null) {
            try {
                method.invoke(iocMap.get(uri),null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void init() throws ServletException {
        try {
            doScan(packageName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        super.init();
    }

    /*@Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("aaaaaaaa");
        super.init(config);
    }*/

    /**
     * 扫描包
     *
     * @param packageName
     * @throws ClassNotFoundException
     */
    public void doScan(String packageName) throws ClassNotFoundException {
        String path = "D:/project_idea/learn/springMvc/target/classes/";
        //path = path + "/springMvc/src/main/java/";
        path = path + packageName.replaceAll("\\.", "/");
        System.out.println(path);
        File file = new File(path);
        //加载class
        loadClassNames(file);
    }

    /**
     * 加载所有类
     *
     * @param file
     */
    public void loadClassNames(File file) throws ClassNotFoundException {
        for (File f : file.listFiles()) {
            if (f.isFile()) {
                //Class cl = this.getClass().getClassLoader().loadClass(f.getAbsolutePath());
                String className = packageName + "." + f.getName().split("\\.")[0];
                System.out.println("className:" + className);
                classNames.add(className);
            } else {
                loadClassNames(f);
            }
        }
        loadMapping();
    }

    /**
     * 遍历每个方法并存储
     *
     * @throws ClassNotFoundException
     */
    private void loadMapping() throws ClassNotFoundException {
        String baseRequestUrl;
        for (String className : this.classNames) {
            Class cl = Class.forName(className);
            for (Annotation annotation : cl.getAnnotations()) {
                if (annotation.annotationType().equals(MyController.class)) {
                    MyRequestMapping myRequestMapping = (MyRequestMapping) cl.getAnnotation(MyRequestMapping.class);
                    if (myRequestMapping != null) {
                        baseRequestUrl = myRequestMapping.value();
                    } else {
                        baseRequestUrl = "";
                    }
                    System.out.println("baseRequestUrl:" + baseRequestUrl);
                    bindMapping(cl, baseRequestUrl);
                }
            }
        }
    }

    /**
     * 绑定mapping
     *
     * @param cl        类
     * @param baseRequestUrl 请求头
     */
    private void bindMapping(Class cl, String baseRequestUrl){
        Object instance = null;
        try {
            instance = cl.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Method[] methods = cl.getMethods();
        for (Method method : methods) {
            MyRequestMapping requestMapping = method.getAnnotation(MyRequestMapping.class);
            if (requestMapping != null) {
                mappings.add(baseRequestUrl + requestMapping.value());
                methodMap.put(baseRequestUrl + requestMapping.value(), method);
                iocMap.put(baseRequestUrl + requestMapping.value(),instance);
            }
        }
    }
}
