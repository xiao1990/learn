package com.fumeck.mvc.controller;

import com.fumeck.mvc.annotation.MyController;
import com.fumeck.mvc.annotation.MyRequestMapping;

@MyController
public class BaseController {

    @MyRequestMapping("/test")
    public void test(){
        System.out.println("it is ok");
    }
}
