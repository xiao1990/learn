package com.fumeck.mvc.controller;

import com.fumeck.mvc.annotation.MyController;
import com.fumeck.mvc.annotation.MyRequestMapping;

@MyController
@MyRequestMapping("/core")
public class CoreController {

    @MyRequestMapping("/test")
    public void test(){
        System.out.println("it is core");
    }
}
