package com.fumeck.mvc.test.handlerMapping;

import com.fumeck.mvc.annotation.MyController;
import com.fumeck.mvc.annotation.MyRequestMapping;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MappingTest {
    private static Logger logger = LoggerFactory.getLogger(MappingTest.class);
    private static final String packageName = "com.fumeck.mvc.controller";
    private List<String> classNames = new ArrayList<>();
    private List<String> mappings = new ArrayList<>();
    private Map<String, Method> methodMap = new HashMap<>();

    public static void main(String[] args) throws ClassNotFoundException {
        MappingTest mappingTest = new MappingTest();
        mappingTest.doScan(packageName);
        for (String mapping : mappingTest.mappings) {
            logger.info(mapping);
            System.out.println(mapping);
        }
    }

    @Test
    public void test() throws ClassNotFoundException {
        doScan(packageName);
        for (String mapping : mappings) {
            System.out.println("mapping:" + mapping);
        }
    }

    /**
     * 扫描包
     *
     * @param packageName
     * @throws ClassNotFoundException
     */
    public void doScan(String packageName) throws ClassNotFoundException {
        String path = "D:/project_idea/learn/springMvc/target/classes/";
        //path = path + "/springMvc/src/main/java/";
        path = path + packageName.replaceAll("\\.", "/");
        System.out.println(path);
        File file = new File(path);
        //加载class
        loadClassNames(file);
    }

    /**
     * 加载所有类
     *
     * @param file
     */
    public void loadClassNames(File file) throws ClassNotFoundException {
        for (File f : file.listFiles()) {
            if (f.isFile()) {
                //Class cl = this.getClass().getClassLoader().loadClass(f.getAbsolutePath());
                String className = packageName + "." + f.getName().split("\\.")[0];
                System.out.println("className:" + className);
                classNames.add(className);
            } else {
                loadClassNames(f);
            }
        }
        loadMapping();
    }

    /**
     * 遍历每个方法并存储
     *
     * @throws ClassNotFoundException
     */
    private void loadMapping() throws ClassNotFoundException {
        String baseRequestUrl;
        for (String className : this.classNames) {
            Class cl = Class.forName(className);
            for (Annotation annotation : cl.getAnnotations()) {
                if (annotation.annotationType().equals(MyController.class)) {
                    MyRequestMapping myRequestMapping = (MyRequestMapping) cl.getAnnotation(MyRequestMapping.class);
                    if (myRequestMapping != null) {
                        baseRequestUrl = myRequestMapping.value();
                    } else {
                        baseRequestUrl = "";
                    }
                    System.out.println("baseRequestUrl:" + baseRequestUrl);
                    bindMapping(cl.getMethods(), baseRequestUrl);
                }
            }
        }
    }

    /**
     * 绑定mapping
     *
     * @param methods        方法数组
     * @param baseRequestUrl 请求头
     */
    private void bindMapping(Method[] methods, String baseRequestUrl) {
        for (Method method : methods) {
            MyRequestMapping requestMapping = method.getAnnotation(MyRequestMapping.class);
            if (requestMapping != null) {
                mappings.add(baseRequestUrl + requestMapping.value());
                methodMap.put(baseRequestUrl + requestMapping.value(), method);
            }
        }
    }
}
