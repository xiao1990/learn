package com.fumeck.mvc.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Test test = new Test();
        Method method = test.getClass().getMethod("say", String.class,Integer.class);
        System.out.println(method.toString());
        System.out.println(method.getDeclaringClass());
        method.invoke(test, new Object[]{"aa",22});
    }

    public void say(String aa, Integer bb) {
        System.out.println("hello:" + aa + bb);
    }
}
