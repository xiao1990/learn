package com.fumeck.design.facade;

public class Client {
    public static void main(String[] args) {
        Facade facade = new Facade();
        //只需调用外观方法A，具体的内部细节我不管
        facade.methodA();
    }
}
