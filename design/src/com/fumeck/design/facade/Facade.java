package com.fumeck.design.facade;

/**
 * 外观类
 */
public class Facade {
    private SubSystemA subSystemA;
    private SubSystemB subSystemB;
    private SubSystemC subSystemC;

    public Facade() {
        subSystemA = new SubSystemA();
        subSystemB = new SubSystemB();
        subSystemC = new SubSystemC();
    }

    public void methodA(){
        subSystemA.methodOne();
        subSystemB.methodTwo();
    }
    public void methodB(){
        subSystemB.methodTwo();
        subSystemC.methodThree();
    }
}
