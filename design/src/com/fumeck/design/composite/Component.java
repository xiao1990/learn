package com.fumeck.design.composite;

/**
 * 组件接口
 */
public interface Component {
    void add(Component component);
    void remove(Component component);
    void excute(int depth);


    /**
     *
     * @param number
     * @return
     * 格式化字符串，左对齐
     */
     static String lpad(int number)
    {
        String f = "";
        for (int i = 0; i < number; i++) {
            f+="-";
        }
        return f;
    }
}
