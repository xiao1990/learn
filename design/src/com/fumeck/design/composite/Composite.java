package com.fumeck.design.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * 枝节点
 */
public class Composite implements Component {
    private String name;

    public Composite(String name) {
        this.name = name;
    }

    private List<Component> cs = new ArrayList<>();

    @Override
    public void add(Component component) {
        cs.add(component);
    }

    @Override
    public void remove(Component component) {
        cs.remove(component);
    }

    @Override
    public void excute(int depth) {
        System.out.println(Component.lpad(depth)+name);
        for (Component c : cs) {
            c.excute(depth + depth);
        }
    }

}
