package com.fumeck.design.composite;

public class Client {
    public static void main(String[] args) {
        Component root = new Composite("根部门");
        //第二层-部门及其下小组
        Component second = new Composite("二层部门");
        second.add(new Leaf("secondLeaf1"));
        second.add(new Leaf("secondLeaf2"));
        root.add(second);
        //第二层-小组
        Component second_2 = new Leaf("二层小组");
        root.add(second_2);
        //第二层-部门的部门等
        Component second_3 = new Composite("二层部门2");
        Component thrid = new Composite("三层部门子部门");
        thrid.add(new Leaf("thridLeaf1"));
        thrid.add(new Leaf("thridLeaf2"));
        second_3.add(thrid);
        root.add(second_3);
        root.excute(2);
    }
}
