package com.fumeck.design.flyweight;

import java.util.HashMap;
import java.util.Map;

public class FlyWeightFactory {
    private Map<String, FlyWeight> map = new HashMap();

    public FlyWeight getFlyWeight(String name) {
        FlyWeight flyWeight = map.get(name);
        if (flyWeight != null) {
            return map.get(name);
        } else {
            flyWeight = new ConcreteFlyWeight(name);
            map.put("name", flyWeight);
            return flyWeight;
        }
    }
}
