package com.fumeck.design.flyweight;

public abstract class FlyWeight {
    private String name;

    public FlyWeight(String name) {
        this.name = name;
    }

    public abstract void excute();
}
