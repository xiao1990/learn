package com.fumeck.design.flyweight;

public class UnShareFlyWeight extends FlyWeight {
    public UnShareFlyWeight(String name) {
        super(name);
    }

    @Override
    public void excute() {

    }
}
