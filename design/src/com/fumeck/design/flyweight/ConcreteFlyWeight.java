package com.fumeck.design.flyweight;

public class ConcreteFlyWeight extends FlyWeight{
    public ConcreteFlyWeight(String name) {
        super(name);
    }

    @Override
    public void excute() {
        //具体事件
    }
}
